import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button, Paper, Typography } from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';

interface WeatherForecast {
    id: 0;
    data: '';
    temperatureC: 0;
    temperatureF: 0;
    summary: '';
}

function App() {
    const header: GridColDef[] = [
        { field: 'id', width: 50 },
        { field: 'date', headerName: 'Date', width: 100 },
        { field: 'temperatureC', headerName: 'Temperature (C)', width: 140 },
        { field: 'temperatureF', headerName: 'Temperature (F)', width: 140 },
        { field: 'summary', headerName: 'Summary', width: 100 },
    ];

    const [content, setContent] = useState<WeatherForecast[]>([]);

    const getContent = () => fetch("https://localhost:7150/WeatherForecast")
        .then(async response => {
            const data = await response.json();
            setContent(data);
        });

    return (
        <>
            <Typography variant='h5' fontFamily="fantasy">Ну и погода...</Typography>
            <Paper style={{ height: 400, width: 600 }}>
                <DataGrid
                    rows={content}
                    columns={header}
                    initialState={{
                        pagination: {
                            paginationModel: {
                                pageSize: 5,
                            },
                        },
                    }}
                />
            </Paper>
            <Button variant='contained' onClick={getContent}>GET</Button>
        </>
    );
}

export default App;
